package cn.uncode.springcloud.starter.security.auth;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * 用户实体
 *
 * @author Juny
 */
@Data
public class EmbedUser implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String USER_TYPE_USER = "USER";
    public static final String USER_TYPE_OPT = "OPT";
    public static final String USER_TYPE_ERP = "ERP";


    /**
     * 用户编号
     */
    private long userId;
    /**
     * 登陆名称
     */
    private String loginName;
    /**
     * 用户类型
     * USER
     * OPT
     * ERP
     */
    private String userType;
    /**
     * 登陆sessionId
     */
    private String userToken;
    /**
     * 登陆时间
     */
    private Date loginTime;

    /**
     * 角色名,以,分割(给@Permission 使用)
     */
    private String roleName;

    /**
     * 允许访问的URL(SecurityInterceptor使用)
     */
    private List<String> urls = new ArrayList<>();

}
