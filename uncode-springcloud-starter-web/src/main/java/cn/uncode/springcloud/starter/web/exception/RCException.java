package cn.uncode.springcloud.starter.web.exception;

import cn.uncode.springcloud.starter.web.result.ResultCode;
import lombok.Getter;

/**
 * 内部code异常
 *
 * @author Juny
 */
public class RCException extends RuntimeException {
	private static final long serialVersionUID = 2359767895161832954L;

	@Getter
	private final ResultCode resultCode;

	public RCException(String message) {
		super(message);
		this.resultCode = ResultCode.INTERNAL_SERVER_ERROR;
	}

	public RCException(ResultCode resultCode) {
		super(resultCode.getMessage());
		this.resultCode = resultCode;
	}

	public RCException(ResultCode resultCode, Throwable cause) {
		super(resultCode.getMessage(), cause);
		this.resultCode = resultCode;
	}

	@Override
	public Throwable fillInStackTrace() {
		return this;
	}
}
