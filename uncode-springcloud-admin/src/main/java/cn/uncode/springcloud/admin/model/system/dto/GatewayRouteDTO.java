package cn.uncode.springcloud.admin.model.system.dto;

import java.util.Date;

import cn.uncode.dal.annotation.Field;
import cn.uncode.dal.annotation.Table;
import lombok.Data;
/**
 * 数据库实体类,此类由Uncode自动生成
 * @author uncode
 * @date 2019-05-28
 */
@Data
@Table(name = "gateway_route")
public class GatewayRouteDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public final static String TABLE_NAME = "gateway_route";
	public final static String ID = "id";
	public final static String ROUTE_ID = "route_id";
	public final static String ROUTE_URI = "route_uri";
	public final static String ROUTE_ORDER = "route_order";
	public final static String PREDICATES = "predicates";
	public final static String FILTERS = "filters";
	public final static String STATUS = "status";
	public final static String CREATE_TIME = "create_time";
	public final static String UPDATE_TIME = "update_time";

	/**
	 * 主键ID
	 */
	private Long id;
	/**
	 * 路由id
	 */
	 @Field(name = "route_id")
	private String routeId;
	/**
	 * 转发目标uri
	 */
	 @Field(name = "route_uri")
	private String routeUri;
	/**
	 * 路由执行顺序
	 */
	 @Field(name = "route_order")
	private Integer routeOrder;
	/**
	 * 断言字符串集合，字符串结构：[{"name":"Path","args":{"pattern" : "/zy/**"}}]
	 */
	private String predicates;
	/**
	 * 过滤器字符串集合，字符串结构：{"name":"StripPrefix","args":{"_genkey_0":"1"}}
	 */
	private String filters;
	/**
	 * 状态（1:启用，2：禁用，3：删除）
	 */
	private int status;
	/**
	 * 创建时间
	 */
	 @Field(name = "create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	 @Field(name = "update_time")
	private Date updateTime;


}