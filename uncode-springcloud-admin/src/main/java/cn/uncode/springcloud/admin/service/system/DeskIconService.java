package cn.uncode.springcloud.admin.service.system;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.uncode.dal.criteria.QueryCriteria;
import cn.uncode.springcloud.admin.common.AppConstants;
import cn.uncode.springcloud.admin.dal.system.DeskIconDAL;
import cn.uncode.springcloud.admin.model.system.bo.DeskIconBO;
import cn.uncode.springcloud.admin.model.system.dto.DeskIconDTO;
import cn.uncode.springcloud.starter.web.result.P;
import cn.uncode.springcloud.utils.obj.BOUitl;
import cn.uncode.springcloud.utils.tree.TreeNodeUitl;

@Service
public class DeskIconService {
	
	@Autowired
	private DeskIconDAL deskIconDAL;
	
	public List<DeskIconBO> loadDeskIconListByUser() {
		QueryCriteria queryCriteria = new QueryCriteria();
		queryCriteria.setRecordIndex(0);
		queryCriteria.setLimit(100);
		queryCriteria.createCriteria().andColumnEqualTo(DeskIconDTO.MENU_TYPE, AppConstants.DESK_ICON_MENU_TYPE_ON_DESK)
		.andColumnEqualTo(DeskIconDTO.STATUS, AppConstants.DESK_ICON_MENU_STATUS_VALID);
		queryCriteria.setAscOrderByClause(DeskIconDTO.ORDER);
		List<DeskIconDTO> list = deskIconDAL.getListByCriteria(queryCriteria);
		return BOUitl.dto2boList(list, DeskIconBO.class);
	}
	
	public List<DeskIconBO> loadStartMenuListByUser() {
		QueryCriteria queryCriteria = new QueryCriteria();
		queryCriteria.setRecordIndex(0);
		queryCriteria.setLimit(100);
		queryCriteria.createCriteria().andColumnEqualTo(DeskIconDTO.MENU_TYPE, AppConstants.DESK_ICON_MENU_TYPE_START_MENU)
		.andColumnEqualTo(DeskIconDTO.STATUS, AppConstants.DESK_ICON_MENU_STATUS_VALID);
		queryCriteria.setAscOrderByClause(DeskIconDTO.ORDER);
		List<DeskIconDTO> list = deskIconDAL.getListByCriteria(queryCriteria);
		List<DeskIconBO> boList = BOUitl.dto2boList(list, DeskIconBO.class);
		boList = TreeNodeUitl.buildTree(boList);
		return boList;
	}
	
	
	public P<List<DeskIconDTO>> getPageList(int pageIndex, int pageSize) {
    	QueryCriteria queryCriteria = new QueryCriteria();
    	if(pageIndex > 0) {
    		queryCriteria.setPageIndex(pageIndex);
    	}
    	if(pageSize > 0) {
    		queryCriteria.setPageSize(pageSize);
    	}
    	queryCriteria.setAscOrderByClause(DeskIconDTO.STATUS+","+DeskIconDTO.ORDER);
		Map<String, Object> resultMap = deskIconDAL.getPageByCriteria4Map(queryCriteria);
		return P.valueOfDAL(resultMap, DeskIconDTO.class);
    }
    
    public long add(DeskIconDTO dto) {
    	Date time = new Date();
    	dto.setCreateTime(time);
        return (Long) deskIconDAL.insertEntity(dto);
    }

    public int update(DeskIconDTO dto) {
        return deskIconDAL.updateEntityById(dto);
    }

    public int delete(List<Long> ids) {
    	QueryCriteria queryCriteria = new QueryCriteria();
		queryCriteria.createCriteria().andColumnIn(DeskIconDTO.ID, ids);
        return deskIconDAL.deleteEntityByCriteria(queryCriteria);
    }

	

}
