package cn.uncode.springcloud.admin.model.system.bo;

import java.util.ArrayList;
import java.util.List;

import cn.uncode.springcloud.admin.model.system.dto.DeskIconDTO;
import cn.uncode.springcloud.utils.obj.BaseBO;
import cn.uncode.springcloud.utils.tree.TreeNode;

import lombok.Data;
/**
 * 数据库实体类,此类由Uncode自动生成
 * @author uncode
 * @date 2019-05-15
 */
@Data
public class DeskIconBO implements TreeNode, BaseBO<DeskIconDTO> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	/**
	 * 主键ID
	 */
	private Long id;
	/**
	 * 桌面图标名字
	 */
	private String name;
	/**
	 * 图标打开窗口的请求地址
	 */
	private String pageURL;
	/**
	 * 窗口类型
	 */
	private Integer openType;
	/**
	 * 窗口标题
	 */
	private String title;
	/**
	 * 桌面图标路径
	 */
	private String icon;
	
	private Long pid;
	
	/**
	 * 窗口打开最大化
	 * <pre>
	 * -1 禁用窗口打开最大化
	 * 1 窗口打开后手动调用最大化方法，前置条件max:true。会有动画延迟，效果不是很好。（不推荐）
	 * 2 以铺满的宽高打开窗口，并非真正意义上的最大化。（推荐）
	 * </pre>
	 */
	private int maxOpen = -1;
	
	private boolean extend = false;
	
	private List<TreeNode> childs;

	@Override
	public long getId() {
		return id;
	}

	@Override
	public long getParentId() {
		return pid;
	}

	@Override
	public void setChildrens(List childrenNodes) {
		childs = childrenNodes;
	}
	
	public static DeskIconBO valueOf(DeskIconDTO dto) {
		DeskIconBO bo = new DeskIconBO();
		bo.valueFromDTO(dto);
		return bo;
	}
	
	public static List<DeskIconBO> valueOf(List<DeskIconDTO> list) {
		if(list != null) {
			List<DeskIconBO> boList = new ArrayList<>();
			for(DeskIconDTO dto:list) {
				DeskIconBO bo = new DeskIconBO();
				bo.valueFromDTO(dto);
				boList.add(bo);
			}
			return boList;
		}
		return null;
	}

	@Override
	public void valueFromDTO(DeskIconDTO dto) {
		this.id = dto.getId();
		this.name = dto.getName();
		this.pageURL = dto.getUrl();
		this.openType = dto.getOpenType();
		this.title = dto.getTitle();
		this.icon = dto.getIcon();
		this.pid = dto.getPid();
	}

	@Override
	public List<TreeNode> getChildrens() {
		return childs;
	}

	@Override
	public DeskIconDTO toDTO() {
		// TODO Auto-generated method stub
		return null;
	}



}