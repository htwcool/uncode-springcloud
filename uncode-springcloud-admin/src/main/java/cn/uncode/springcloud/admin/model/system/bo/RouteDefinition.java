package cn.uncode.springcloud.admin.model.system.bo;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import cn.uncode.springcloud.admin.model.system.dto.GatewayRouteDTO;
import cn.uncode.springcloud.utils.json.JsonUtil;
import cn.uncode.springcloud.utils.obj.BaseBO;

import lombok.Data;

/**
 * Gateway的路由定义模型
 */
@Data
public class RouteDefinition implements BaseBO<GatewayRouteDTO>{

    /**
	 * 
	 */
	private static final long serialVersionUID = -8925071673321013834L;

	/**
     * 路由的Id
     */
    private String id;

    /**
     * 路由断言集合配置
     */
    private List<PredicateDefinition> predicates = new ArrayList<>();

    /**
     * 路由过滤器集合配置
     */
    private List<FilterDefinition> filters = new ArrayList<>();

    /**
     * 路由规则转发的目标uri
     */
    private String uri;

    /**
     * 路由执行的顺序
     */
    private int order = 0;

	@Override
	public void valueFromDTO(GatewayRouteDTO dto) {
		this.id = dto.getRouteId();
		this.uri = dto.getRouteUri();
		this.order = dto.getRouteOrder();
		if(StringUtils.isNotBlank(dto.getPredicates())){
			List<PredicateDefinition> list = JsonUtil.parseArray(dto.getPredicates(), PredicateDefinition.class);
			this.setPredicates(list);
		}
		if(StringUtils.isNotBlank(dto.getFilters())){
			List<FilterDefinition> list = JsonUtil.parseArray(dto.getFilters(), FilterDefinition.class);
			this.setFilters(list);
		}
	}

	@Override
	public GatewayRouteDTO toDTO() {
		// TODO Auto-generated method stub
		return null;
	}

    
}
