package cn.uncode.springcloud.admin.controller.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.uncode.springcloud.admin.model.system.bo.DeskIconBO;
import cn.uncode.springcloud.admin.service.system.DeskIconService;
import cn.uncode.springcloud.starter.web.result.R;


/**
 * 桌面图标
 * @author juny
 * @date 2019年5月13日
 *
 */
@RestController
@RequestMapping("/sys")
public class SysController{
	
	@Autowired
	DeskIconService deskIconService;
	
	@RequestMapping("/loadDeskIcons")
    public R<List<DeskIconBO>> loadDeskIcons() {
    	List<DeskIconBO> list = deskIconService.loadDeskIconListByUser();
    	return R.success(list);
    }
	
	@RequestMapping("/loadStartMenus")
    public R<List<DeskIconBO>> loadStartMenus() {
    	List<DeskIconBO> list = deskIconService.loadStartMenuListByUser();
    	return R.success(list);
    }


}